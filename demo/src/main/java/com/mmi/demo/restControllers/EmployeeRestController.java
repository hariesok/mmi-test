package com.mmi.demo.restControllers;

import com.mmi.demo.services.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/employee", produces = "application/json")
@CrossOrigin(origins = "*")
public class EmployeeRestController {
    @Autowired
    private EmployeeService employees;

    @GetMapping("/")
    public ResponseEntity<?> getAllEmployee() {
        return new ResponseEntity<>(employees.getAllEmployee(), HttpStatus.OK);
    }
}