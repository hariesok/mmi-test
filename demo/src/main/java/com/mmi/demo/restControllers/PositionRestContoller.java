package com.mmi.demo.restControllers;

import com.mmi.demo.services.PositionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/api/position", produces="application/json")
@CrossOrigin(origins = "*")
public class PositionRestContoller {
    @Autowired
    private PositionService positionservice;
    @GetMapping("/")
    public ResponseEntity<?>getAllPosition() {
        return new ResponseEntity<>(positionservice.getAllPosition(), HttpStatus.OK);
    }
}