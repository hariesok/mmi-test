package com.mmi.demo.serviceImpl;

import java.util.List;

import com.mmi.demo.models.PositionModel;
import com.mmi.demo.repositories.PositionRepository;
import com.mmi.demo.services.PositionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    private PositionRepository positionrepo;

    @Override
    public List<PositionModel> getAllPosition() {
        return positionrepo.getAllPosition();
    }
    
}