package com.mmi.demo.services;

import java.util.List;
import java.util.Optional;

import com.mmi.demo.models.EmployeeModel;

public interface EmployeeService {
    List<EmployeeModel> getAllEmployee();
    void delete(int id);
    EmployeeModel save(EmployeeModel Employee);
    Optional<EmployeeModel> findById(int id);

    //Not mandatory: filter search employee
    List<EmployeeModel> findEmployee(String name, int idNumber);
}