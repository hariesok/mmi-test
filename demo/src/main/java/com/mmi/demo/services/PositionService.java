package com.mmi.demo.services;

import java.util.List;

import com.mmi.demo.models.PositionModel;

public interface PositionService {
    List<PositionModel> getAllPosition();
}