package com.mmi.demo.repositories;

import java.util.List;

import com.mmi.demo.models.PositionModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<PositionModel, Integer>{
    @Query("select p from PositionModel p where isDelete = 0")
    List<PositionModel> getAllPosition();
}