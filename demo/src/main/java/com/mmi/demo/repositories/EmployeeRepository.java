package com.mmi.demo.repositories;

import java.util.List;

import com.mmi.demo.models.EmployeeModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeModel, Integer> {
    @Query("select e from EmployeeModel e where isDelete = 0")
    List<EmployeeModel> getAllEmployee();

    @Query("select e from EmployeeModel e where isDelete = 0 and (e.name like %?1% and e.idNumber = ?2)")
    List<EmployeeModel> findEmployee(String name, int idNumber);
}